# Fonds d'Archives Eduardo-Manet

Influencé par sa mère francophile, Eduardo Manet (La Havane, 1930) s’intéresse dès son plus jeune âge à la culture française. Il vit à Paris à partir de 1951 et suit plusieurs formations, dont des études de littérature à La Sorbonne et des cours à l’École internationale de théâtre et de mime de Jacques Lecoq. Sollicité par les autorités culturelles cubaines après la révolution de 1959, il retourne à Cuba en 1960, où il devient vite une figure importante du milieu culturel local. Manet est nommé directeur du Centre des arts dramatiques et réalise une dizaine de films, avant de quitter le pays déçu par le tournant communiste de la Révolution cubaine. Grâce à Roger Blin – qui lit le manuscrit de sa pièce de théâtre Les Nonnes et décide de la mettre en scène –, il s’installe en France en 1968. Il entame dès lors une carrière littéraire remarquée. Ses romans, écrits en français, obtiennent d’importantes récompenses, dont le Prix Goncourt des lycéens pour *L’île du lézard vert* (1992) et le Prix Interallié pour *Rhapsodie cubaine* (1996).

Ce site a été généré avec Nakala-Quarto-View, un générateur de site web statique "maison" écrit en langage Python, utilisant l'API de Nakala pour requêter les données et Quarto pour construire le site.
Le rendu de ce site est visible ici: [https://mshs-poitiers.gitpages.huma-num.fr/crla/eduardo-manet/](https://mshs-poitiers.gitpages.huma-num.fr/crla/eduardo-manet/)

Le code source et les explications sont disponibles ici: [https://gitlab.huma-num.fr/mshs-poitiers/plateforme/nakala-quarto-view](https://gitlab.huma-num.fr/mshs-poitiers/plateforme/nakala-quarto-view)

## Localisation des données
La collection des données est disponible sur [nakala.fr](https://nakala.fr/search/?q=eduardo+manet)
Il existe un autre site de visualisation des mêmes données, réalisé avec NakalaPress: [https://archives-eduardo-manet.nakala.fr/](https://archives-eduardo-manet.nakala.fr/)

## fichiers spécifiques
le dossier env_quarto contient des fichiers spécifiques utilisés pour construire le site: contenu de pages d'accueil et "à propos", par exemple, et les images "fixes" utilisées dans ces pages.
